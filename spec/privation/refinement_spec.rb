RSpec.describe Privation::Refinement do
  using Privation::Refinement

  class SampleObject
    privation_apply
    privation_expose :exposed_method

    def unscoped_method
      true
    end

    def exposed_method
      true
    end
  end

  subject { SampleObject.new }

  it "marks public methods as private" do
    expect { subject.unscoped_method }.to raise_error(NoMethodError, /^private method `unscoped_method' called/)
  end

  it "exposes methods specified via `privation_expose`" do
    expect(subject.exposed_method).to be_truthy
  end
end
