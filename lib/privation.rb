require "privation/version"
require "set"

module Privation
  class Error < StandardError; end

  module Extension
    def privation_apply
      self.class_eval do
        def self.method_added(method_name)
          unless @@privation_interface.include? method_name
            private method_name
          end
        end
      end
    end

    def privation_expose(*args)
      @@privation_interface ||= Set[]
      @@privation_interface.merge(args.each(&:to_s).each(&:to_sym))
    end
  end

  module Refinement
    refine Object do
      include Privation::Extension
    end
  end
end
