# Privation

[![pipeline status](https://gitlab.com/plainprogrammer/privation/badges/master/pipeline.svg)](https://gitlab.com/plainprogrammer/privation/commits/master)
[![coverage report](https://gitlab.com/plainprogrammer/privation/badges/master/coverage.svg)](https://gitlab.com/plainprogrammer/privation/commits/master)

Being explicit when designing a class' public interface is an often neglected
aspect of design for many programmers. The Privation gem forces more explicit
consideration of a class' public interface by requiring methods to be made
public by explicit declaration, rather than Ruby's default of making all
instance methods public.

## Installation

Add this line to your application's Gemfile:

```ruby
gem "privation"
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install privation

## Usage

Privation relies on a refinement to Ruby's basic Object class, extending it
with two methods that allow privation to be used within classes you define. To
make use of Privation you first have to specify that you want to use the
`Privation::Refinement`. Once you have declared the use of the refinement you
are able to use both `privation_apply`, which activates Privation for a
specific class, and `privation_expose`, which allows the definition of your
class' intended public interface.

```ruby
using Privation::Refinement

class MyAwesomeThing
  privation_apply
  privation_expose :exposed_method

  def exposed_method
    # ...
  end

  def unexposed_method
    # ...
  end 
end
```

### Applying Globally

*CAUTION:* You should be very careful applying Privation globally. In an
established project this will almost certainly break things. It may be
reasonable to start a project with Privation applied. But, you should
definitely consider whether you really want private method scoping as your
global default.

```ruby
class Object
  include Privation::Extension
end
```

Add the above code in the early stages of your code's execution to gain global
access to the `privation_apply` and `privation_expose` methods.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/privation. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Privation project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/privation/blob/master/CODE_OF_CONDUCT.md).
